﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneController : MonoBehaviour
{

	// Use this for initialization
	void Start ()
    {
        //Find all jumping spheres and randomize their jumping animation offset
        Animator[] allJumpingObjects = (Animator[])GameObject.FindObjectsOfType(typeof(Animator));
        foreach(Animator jumpingObject in allJumpingObjects)
        {
            jumpingObject.SetFloat("jumpOffsetTime", Random.Range(0f, 1f));
        }
    }

    public GameObject spawnObj;

    void Update()
    {
        //Spawn object at the clicked position
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 mousePos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0f);
            Vector3 worldPos;
            Ray ray = Camera.main.ScreenPointToRay(mousePos);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 1000f))
            {
                worldPos = hit.point;
                Vector3 spawnPos = new Vector3(worldPos.x, 0.5f, worldPos.z);
                Instantiate(spawnObj, spawnPos, Quaternion.identity);
            }
        }
    }
}
