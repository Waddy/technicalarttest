﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpingSphere : MonoBehaviour
{
	// Use this for initialization
	void Start ()
    {
        Animator sphereAnimator = GetComponent<Animator>();
        if(sphereAnimator != null)
        {
            sphereAnimator.SetFloat("jumpOffsetTime", Random.Range(0f, 1f));
        }

        Transform particle_system = transform.Find("FX_spawn");
        if(particle_system != null)
        {
            Destroy(particle_system.gameObject, 3.0f);
        }
    }
}
