﻿using UnityEngine;
using UnityEditor;

public class SceneMaterialEditor : EditorWindow
{
    string m_ObjName;
    Material m_Material;

    [MenuItem("Window/Scene Material Editor")]
    public static void ShowWindow()
    {
        GetWindow<SceneMaterialEditor>("Scene Material");
    }

    void OnGUI()
    {
        GUILayout.Label("Change the material of all game objects with a defined name in the scene", EditorStyles.boldLabel);

        m_ObjName = EditorGUILayout.TextField("Name", m_ObjName);
        m_Material = EditorGUILayout.ObjectField("Material", m_Material, typeof(Material), false) as Material;

        if (GUILayout.Button("Change Material"))
        {
            ChangeMaterial();
        }
    }

    void ChangeMaterial()
    {
        GameObject[] allObjects = (GameObject[])GameObject.FindObjectsOfType(typeof(GameObject));
        foreach (GameObject obj in allObjects)
        {
            if (obj.name == m_ObjName)
            {
                MeshRenderer renderer = obj.GetComponent<MeshRenderer>();
                if (renderer != null)
                {
                    renderer.sharedMaterial = m_Material;
                }
            }
        }
    }

}